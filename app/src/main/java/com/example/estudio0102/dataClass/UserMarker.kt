package com.example.estudio0102.dataClass

data class UserMarker(
    val Nombre: String,
    val latitud: Double,
    val longitud: Double
)
