package com.example.estudio0102

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import com.example.estudio0102.dataClass.UserMarker
import com.example.estudio0102.databinding.ActivityLogInBinding
import com.example.estudio0102.viewModel.CamperViewModel
import com.google.firebase.auth.FirebaseAuth

class LogInActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLogInBinding
    private val locationViewModel: CamperViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLogInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val email = binding.input1.text
        val password = binding.input2.text


        binding.buttonRegister.setOnClickListener {
            println("Email sin ToString()--> "+email)
            println(password)
            createUser(email.toString(), password.toString())
        }

        binding.buttonLogIn.setOnClickListener {
            logInUser(email.toString(),password.toString())
        }
    }

    private fun showError(string: String){
        println(string)
    }

    private fun goToHome(emailLogged: String){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
    }

    private fun createUser(email:String, password:String){
        println("INSIDE CREATEUSER FUNCTION")
        FirebaseAuth.getInstance().
        createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if(it.isSuccessful){
                    println("INSIDE IT--> IS SUCCESSFUL")
                    val emailLogged = it.result?.user?.email
                    goToHome(emailLogged!!)
                }
                else{
                    showError("Error al registrar l'usuari")
                }
            }
    }

    private fun logInUser(email: String, password: String){
        FirebaseAuth.getInstance().
        signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if(it.isSuccessful){
                    val emailLogged = it.result?.user?.email
                    goToHome(emailLogged!!)
                }
                else{
                    showError("Error al fer login")
                }
            }
    }

}