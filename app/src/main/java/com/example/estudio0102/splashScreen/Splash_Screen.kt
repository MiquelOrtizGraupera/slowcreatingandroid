package com.example.estudio0102.splashScreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.example.estudio0102.LogInActivity
import com.example.estudio0102.MainActivity
import com.example.estudio0102.R

class Splash_Screen: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?){
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(androidx.core.splashscreen.R.layout.splash_screen_view)
        splashScreen.setKeepOnScreenCondition{true}
        val intent = Intent(this, LogInActivity::class.java)
        startActivity(intent)
        finish()

    }
}