package com.example.estudio0102.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.estudio0102.OnCLickListener
import com.example.estudio0102.R
import com.example.estudio0102.dataClass.ItemRecycler
import com.example.estudio0102.dataClass.UserMarker
import com.example.estudio0102.databinding.ItemLocationBinding

class Adapter(private val locations: MutableList<UserMarker>, private val listener:OnCLickListener): RecyclerView.Adapter<Adapter.ViewHolder>() {
  inner class ViewHolder(view: View):RecyclerView.ViewHolder(view){
      val binding = ItemLocationBinding.bind(view)

      fun setListener(itemRecycler: UserMarker){
          binding.root.setOnClickListener {
              listener.onClick(itemRecycler)
          }
      }
  }

private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_location, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = locations[position]
        with(holder){
            setListener(item)
            binding.nombreLugar.text = item.Nombre
        }
    }

    override fun getItemCount(): Int {
        return locations.size
    }
}