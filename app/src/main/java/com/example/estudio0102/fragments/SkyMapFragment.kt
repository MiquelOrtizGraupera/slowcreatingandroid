package com.example.estudio0102.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.example.estudio0102.R
import com.example.estudio0102.dataClass.ItemRecycler
import com.example.estudio0102.dataClass.UserMarker
import com.example.estudio0102.databinding.SkyMapFragmentBinding
import com.example.estudio0102.viewModel.CamperViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlin.reflect.typeOf

class SkyMapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var currentCoordinates: LatLng
    private val locationViewModel: CamperViewModel by activityViewModels()
    private lateinit var binding: SkyMapFragmentBinding
    private lateinit var map: GoogleMap
    private val REQUEST_CODE_LOCATION = 100
    //Instancia para DDBB
    private val db = FirebaseFirestore.getInstance()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = SkyMapFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }
//
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        getLocation()

        binding.searchBtn.setOnClickListener {
            /*GO TO LOCATION AND ADD MARKER CODE*/
            val location = binding.skyFragmentTextView.text.toString()
            if( location === "Escribe tu lugar..." || location === ""){
                Toast.makeText(this.context, "Type any location please...", Toast.LENGTH_SHORT).show()
            }else{
                val geocoder = Geocoder(this.requireContext(), java.util.Locale.FRENCH)
                val lista = geocoder.getFromLocationName(location, 1) as MutableList<Address>
                if(lista.size > 0){
                    val latLang = LatLng(lista[0].latitude, lista[0].longitude)
                    map.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(latLang, 18f),
                        2000, null)

                    binding.addMarcador.setOnClickListener{
                        val myMarker = MarkerOptions().position(latLang).title(binding.skyFragmentTextView.text.toString())
                        map.addMarker(myMarker)
                        val locationToSave = UserMarker(binding.skyFragmentTextView.text.toString(), lista[0].latitude, lista[0].longitude)
                        locationViewModel.saveMarcadorInToSkyList(locationToSave)


                        db.collection(FirebaseAuth.getInstance().currentUser!!.email.toString()).document(location).set(
                            hashMapOf(
                                "Nombre" to location,
                                "Latitud" to lista[0].latitude,
                                "Longitud" to lista[0].longitude)
                        )
                    }
                }
            }
        }


    }

    //Creacio mapa Google
       private fun createMap(){
            val mapFragment = childFragmentManager.findFragmentById(R.id.googleMaps) as SupportMapFragment?
            mapFragment?.getMapAsync(this)
        }
    //Metode OnMapReadyCallBack
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        enableLocation()
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(currentCoordinates, 18f), 2000, null
        )
        createMarker()
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        if (isLocationPermissionGranted() && !locationViewModel.activoBol) {
            println("PRIMER if GETLOCATION")
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                val location = it.result
                if (location != null) {
                    currentCoordinates = LatLng(location.latitude, location.longitude)
                    createMap()
                }
            }
        }else if(isLocationPermissionGranted() && locationViewModel.activoBol){
            currentCoordinates = LatLng(locationViewModel.selectedLocation.value!!.longitud, locationViewModel.selectedLocation.value!!.latitud)
            createMap()
        }
        else {
            requestLocationPermission()
        }
    }

    //Marcador de mapa
    private fun createMarker(){
        println("DESDE CREATE MARKER"+locationViewModel.activoBol)
        if(locationViewModel.activoBol){
            val myMarker = MarkerOptions().position(currentCoordinates).title(locationViewModel.selectedLocation.value!!.Nombre)
            map.addMarker(myMarker)
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(currentCoordinates, 18f),
                2000, null)
        }else{
            val myMarker = MarkerOptions().position(currentCoordinates).title("You in the map")
            map.addMarker(myMarker)
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(currentCoordinates, 18f),
                2000, null)
        }

    }
    //Comprovar permis de geolocalització
    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
    //Si permisos habilitats --> creem geolocalitzacio
    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }
    //Demanara permis de geolocalització
    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
            getLocation()
        }
    }


}

