package com.example.estudio0102.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.estudio0102.OnCLickListener
import com.example.estudio0102.R
import com.example.estudio0102.dataClass.UserMarker
import com.example.estudio0102.databinding.FragmentDetailBinding
import com.example.estudio0102.viewModel.CamperViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class DetailFragment : Fragment(){
    private val locationViewModel: CamperViewModel by activityViewModels()
    private lateinit var binding: FragmentDetailBinding
    private val db = FirebaseFirestore.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.nombreLugar.text = locationViewModel.retornoLocalizacionGuardada()?.Nombre
        binding.latitudLocation.text = locationViewModel.retornoLocalizacionGuardada()?.latitud.toString()
        binding.longitudLocation.text = locationViewModel.retornoLocalizacionGuardada()?.longitud.toString()
        binding.titleImage.text = locationViewModel.retornoLocalizacionGuardada()?.Nombre

        binding.addFoto.setOnClickListener {
            /**
             * TODO activate Android Camera.
             */
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.nav_host_fragment, CameraFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
                commit()
            }
        }

        binding.goToMap.setOnClickListener {
            println("Desde el DETAIL FRAGMENT" + locationViewModel.activoBol)

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.nav_host_fragment, SkyMapFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
                commit()
            }
        }

        binding.deleteLocation.setOnClickListener {
            db.collection(FirebaseAuth.getInstance().currentUser!!.email.toString()).document(
                locationViewModel.retornoLocalizacionGuardada()?.Nombre.toString()
            ).delete()

            parentFragmentManager.beginTransaction().apply {

                replace(R.id.nav_host_fragment, SkyMapFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
                commit()
            }
        }
    }

}