package com.example.estudio0102.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.estudio0102.LogInActivity
import com.example.estudio0102.MainActivity
import com.example.estudio0102.OnCLickListener
import com.example.estudio0102.R
import com.example.estudio0102.adapter.Adapter
import com.example.estudio0102.dataClass.ItemRecycler
import com.example.estudio0102.dataClass.UserMarker
import com.example.estudio0102.databinding.FragmentFavouritePlacesBinding
import com.example.estudio0102.viewModel.CamperViewModel
import com.firebase.ui.auth.data.model.User
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavouritePlaces : Fragment(), OnCLickListener {

private lateinit var binding: FragmentFavouritePlacesBinding
private lateinit var locationAdapter: Adapter
private lateinit var linearLayoutManager: RecyclerView.LayoutManager
private lateinit var linearLayoutManager2: RecyclerView.LayoutManager
private val viewModel: CamperViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavouritePlacesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        linearLayoutManager = LinearLayoutManager(context)

        viewModel.getFromFireBase()

        viewModel.listFromFireBase.observe(viewLifecycleOwner, Observer{
            setUpRecyclerView(viewModel.getAllSkyPlaces())
        })


        binding.logoutButton.setOnClickListener {
            logOutApp()
        }
    }

    fun logOutApp(){
        println(FirebaseAuth.getInstance().currentUser)
        FirebaseAuth.getInstance().signOut()
        println(FirebaseAuth.getInstance().currentUser)
    }

    override fun onClick(itemRecycler: UserMarker) {
        viewModel.select(itemRecycler)
        if(!viewModel.activoBol){
            viewModel.changeBool()
        }
        parentFragmentManager.beginTransaction().apply {
               replace(R.id.nav_host_fragment, DetailFragment())
               setReorderingAllowed(true)
               addToBackStack(null)
               commit()
           }
    }


    private fun setUpRecyclerView(itemRecycler: MutableList<UserMarker>) {
        locationAdapter = Adapter(itemRecycler, this)
        binding.skyRecycler.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = locationAdapter
        }
    }
}