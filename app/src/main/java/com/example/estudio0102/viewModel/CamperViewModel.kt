package com.example.estudio0102.viewModel

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.estudio0102.dataClass.UserMarker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class CamperViewModel: ViewModel() {
    var selectedLocation = MutableLiveData<UserMarker>()
    private var listaMarcadores = mutableListOf<UserMarker>()
     var listFromFireBase = MutableLiveData<MutableList<UserMarker>>().apply {
        this.value = mutableListOf<UserMarker>()
    }
    private val db = FirebaseFirestore.getInstance()
    var activoBol = false
    init{
        getFromFireBase()
    }

    fun getFromFireBase() {
        val listMutableFireBase = mutableListOf<UserMarker>()
         db.collection(FirebaseAuth.getInstance().currentUser!!.email.toString())
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                     var marker = UserMarker(
                        document.id,document.get("Longitud") as Double, document.get("Latitud") as Double)
                    listMutableFireBase.add(marker)
                }
                listFromFireBase.postValue(listMutableFireBase)
                println(listFromFireBase.value!!.size)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }

    }

    fun getAllSkyPlaces(): MutableList<UserMarker> {
        return listFromFireBase.value!!
    }

    fun saveMarcadorInToSkyList(itemRecycler: UserMarker){
        listaMarcadores.add(itemRecycler)
    }


    fun select(itemRecycler: UserMarker){
        selectedLocation.postValue(itemRecycler)
    }

    fun retornoLocalizacionGuardada(): UserMarker? {
        return selectedLocation.value!!
    }
    fun changeBool():Boolean{
        println(activoBol)
        activoBol = !activoBol
        println(activoBol)
        return activoBol
    }
}